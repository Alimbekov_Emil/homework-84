const express = require("express");
const User = require("../models/User");
const auth = require("../middleWare/auth");
const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const user = await User.find();
    return res.send(user);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post("/", async (req, res) => {
  try {
    const user = new User(req.body);
    user.generateToken();
    await user.save();
    return res.send(user);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post("/sessions", async (req, res) => {
  const user = await User.findOne({ username: req.body.username });

  if (!user) {
    return res.status(401).send({ error: "User Name not Found" });
  }
  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(401).send({ error: "Password is wrong" });
  }
  user.generateToken();
  await user.save();
  return res.send({ token: user.token });
});

module.exports = router;
