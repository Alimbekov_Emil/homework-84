const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const exitHook = require("async-exit-hook");
const tasks = require("./app/tasks");
const users = require("./app/users");

const app = express();
app.use(express.json());
app.use(cors());

const port = 8000;

app.use("/tasks", tasks);
app.use("/users", users);

const run = async () => {
  await mongoose.connect("mongodb://localhost/todo-list", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  exitHook(async (callback) => {
    await mongoose.disconnect();
    console.log("mongoose disconnected");
    callback();
  });
};

run().catch(console.error);
