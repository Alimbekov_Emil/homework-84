const mongoose = require("mongoose");

const TaskSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      requred: true,
    },
    description: String,
    status: {
      type: String,
      requred: true,
      enum: ["new", "in_progress", "complete"],
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
  },
  {
    versionKey: false,
  }
);

const Task = mongoose.model("Task", TaskSchema);

module.exports = Task;
